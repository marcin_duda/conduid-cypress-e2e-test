describe('Login', () => {
    //logowanie
    it('Log in to Conduit', () => {
        // wejście na stronę 
        cy.visit('http://react-redux.realworld.io/#/login?_k=rpwhb8') 
        // sprawdzenie tytułu strony
        cy.title().should('eq', 'Conduit') 
        //sprawdzamy czy element a ma atrybut href rowny #register
        cy.contains('Need an account?').should('have.attr', 'href', '#register')
        //sprawdzenie protokołu https
        cy.location('protocol').should('eq', 'http:')
        // znalezienie pola typu email oraz wpisanie adresu
        cy.get('input[type="email"]').type('geotix.md@gmail.com')
        // znalezienie pola typu password oraz wpisanie hasła - selektor bardziej wrażliwy na przyszłe modyfikacje
        cy.get('fieldset.form-group:nth-child(2) > input:nth-child(1)').type('kolombo7')
        // kliknięcie w przycisk 'Sign in'
        cy.get('.btn').contains('Sign in').click()
        //czekamy na przycisk settings i sprawdzamy czy jest widoczny
        cy.contains('Settings',{timeout:10000}).should('be.visible')
    })
    //  tworzenie nowego posta
    it('Create a new post', () => {
        cy.contains('New Post').click()
        cy.get('input[placeholder="Article Title"]').type('Article title - demo')
        cy.get('input[placeholder="What\'s this article about?"]').type('About - demo')
        cy.get('textarea[placeholder="Write your article (in markdown)"]').type('content - demo')
        cy.get('fieldset > button').click()
        cy.url().should('include', 'article')
    })
    it('mark and unmark favourite', () => {
        cy.get('.nav-link').contains('Marcin').click()
        cy.contains('My Articles').should('be.visible')
        cy.get('.ion-heart').click()
        cy.contains('Favorited Articles').click()
        cy.url().should('include', 'favorites')
        cy.get('.ion-heart').click()
        cy.reload()
        cy.contains('No articles are here... yet.').should('be.visible')
        cy.go('back')
    })
})